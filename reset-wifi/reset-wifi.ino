
#include <Process.h>

void setup() {
  Bridge.begin();   // Initialize the Bridge
  SerialUSB.begin(9600);    // Initialize the Serial

  // Wait until a Serial Monitor is connected.
  while (!SerialUSB);
  Process p;
  p.runShellCommand("/usr/bin/wifi-reset-and-reboot");

  while (p.running());
  Serial.println("Reset completed. Wait some minutes!");

}

void loop() {}
